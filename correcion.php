<?php

    $pass = $_POST['pass'];
    $pass2 = $_POST['pass2'];

    if(strlen($pass) >= 8){
        if($pass == $pass2) echo "<p style='color:green'>Datos procesados correctamente</p>";
        else echo "<p style='color:red'>Error en el formulario: Las contraseñas no coinciden</p>";
    }else echo "<p style='color:red'>Error en el formulario: La contraseña debe tener al menos 8 caracteres</p>";

?>